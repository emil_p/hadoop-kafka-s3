package ru.itis.minio.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "minio")
public class MinioProperties {

    private String host;

    private String bucket;

    private String accessKey;

    private String secretKey;

    private Integer port;

    private Boolean tls;
}
