package ru.itis.minio.service;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import ru.itis.minio.model.KafkaMessage;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.inotify.Event;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.RemoveObjectArgs;
import io.minio.UploadObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class MinioService {

    private final MinioClient minioClient;

    private final FileSystem hdfs;

    @Value("${tempSavingFolder}")
    private String saveFolder;

    @KafkaListener(topics = "${kafka.topic}", containerFactory = "listenerContainerFactory", groupId = "${kafka.groupId}")
    public void processMessage(KafkaMessage kafkaMessage) throws IOException, ServerException, InsufficientDataException,
            NoSuchAlgorithmException, InternalException, InvalidResponseException,
            XmlParserException, InvalidKeyException, ErrorResponseException {
        log.info("Received: " + kafkaMessage);

        if (kafkaMessage.getEvent().equals(Event.EventType.CREATE.name())) {
            uploadFile(kafkaMessage);
        }

        if (kafkaMessage.getEvent().equals(Event.EventType.UNLINK.name())) {
            deleteFile(kafkaMessage);
        }
    }

    private void uploadFile(KafkaMessage kafkaMessage) throws IOException, InvalidResponseException, InvalidKeyException, NoSuchAlgorithmException, ServerException, ErrorResponseException, XmlParserException, InsufficientDataException, InternalException {
        String filename = (new File(kafkaMessage.getSource())).getName();
        String localUrl = saveFolder.concat("/").concat(filename);

        log.info("File name: {}, local path : {}", filename, localUrl);

        createBucketIfNotExists(kafkaMessage.getBucket());

        hdfs.copyToLocalFile(new Path(kafkaMessage.getSource()), new Path(localUrl));

        minioClient.uploadObject(UploadObjectArgs.builder()
                .bucket(kafkaMessage.getBucket())
                .object(filename)
                .filename(localUrl)
                .build());

        log.info("File {} was added to minio", filename);

        boolean result = new File(localUrl).delete();

        log.info("File {} was removed from local folder", filename);

        if (!result) {
            log.error("File {} with path {} wasn`t removed from local folder", filename, localUrl);
            throw new RuntimeException();
        }
    }

    private void deleteFile(KafkaMessage kafkaMessage) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder()
                .bucket(kafkaMessage.getBucket())
                .build();

        if (!minioClient.bucketExists(bucketExistsArgs)) {
            log.info("File {} is not found", kafkaMessage.getSource());
            throw new RuntimeException();
        }

        String fileName = kafkaMessage.getSource()
                .substring(kafkaMessage.getSource().lastIndexOf("/") + 1);

        minioClient.removeObject(RemoveObjectArgs.builder()
                .bucket(kafkaMessage.getBucket())
                .object(fileName)
                .build()
        );

        log.info("File {} was removed from minio", kafkaMessage.getSource());
    }

    private void createBucketIfNotExists(String bucket) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder()
                .bucket(bucket)
                .build();

        if (!minioClient.bucketExists(bucketExistsArgs)) {
            minioClient.makeBucket(MakeBucketArgs.builder()
                    .bucket(bucket)
                    .build()
            );

            log.info("Created bucket with name {}", bucket);
        }
    }
}
