package ru.itis.minio.configuration;

import ru.itis.minio.properties.MinioProperties;

import io.minio.MinioClient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Configuration
public class MinioConfiguration {

    private final MinioProperties minio;

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(minio.getHost(), minio.getPort(), minio.getTls())
                .credentials(minio.getAccessKey(), minio.getSecretKey())
                .build();
    }

}
