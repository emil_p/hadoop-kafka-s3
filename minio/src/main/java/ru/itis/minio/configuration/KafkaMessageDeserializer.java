package ru.itis.minio.configuration;

import ru.itis.minio.model.KafkaMessage;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.header.Headers;

import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.core.type.TypeReference;

public class KafkaMessageDeserializer extends JsonDeserializer<KafkaMessage> {

    @Override
    public KafkaMessage deserialize(String topic, Headers headers, byte[] data) {
        return deserialize(topic, data);
    }

    @Override
    public KafkaMessage deserialize(String topic, byte[] data) {
        if (Objects.isNull(data)) {
            return null;
        }
        try {
            return objectMapper.readValue(data, new TypeReference<KafkaMessage>() {});
        } catch (IOException e) {
            throw new SerializationException("Can't deserialize data [" + Arrays.toString(data) +
                    "] from topic [" + topic + "]", e);
        }
    }
}
