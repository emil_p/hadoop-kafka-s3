package ru.itis.minio.configuration;

import java.io.IOException;

import ru.itis.minio.properties.HdfsProperties;

import org.apache.hadoop.fs.FileSystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Configuration
public class HdfsConfiguration {

    private final HdfsProperties hdfs;

    @Bean
    public FileSystem hdfsClient() throws IOException {
        System.setProperty("HADOOP_USER_NAME", hdfs.getUser());
        org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
        configuration.set("fs.default.name", hdfs.getUrl());
        return FileSystem.get(configuration);
    }

}
