package ru.itis.minio.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan("ru.itis.minio")
public class MinioApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinioApplication.class);
    }
}
