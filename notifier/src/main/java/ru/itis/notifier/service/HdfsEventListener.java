package ru.itis.notifier.service;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import ru.itis.notifier.model.KafkaMessage;
import ru.itis.notifier.properties.HdfsProperties;
import ru.itis.notifier.properties.MinioProperties;

import org.apache.hadoop.hdfs.DFSInotifyEventInputStream;
import org.apache.hadoop.hdfs.inotify.Event;
import org.apache.hadoop.hdfs.inotify.EventBatch;
import org.apache.hadoop.hdfs.inotify.MissingEventsException;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HdfsEventListener {

    private final KafkaService kafkaService;

    private final DFSInotifyEventInputStream hdfsInputStream;

    private final HdfsProperties hdfsProperties;

    private final MinioProperties minioProperties;

    private final Cache<String, KafkaMessage> fileMap;

    private final static int FILE_CACHE_HOUR = 1;

    public HdfsEventListener(KafkaService kafkaService,
                             DFSInotifyEventInputStream hdfsInputStream,
                             HdfsProperties hdfsProperties,
                             MinioProperties minioProperties) {
        this.kafkaService = kafkaService;
        this.hdfsInputStream = hdfsInputStream;
        this.hdfsProperties = hdfsProperties;
        this.minioProperties = minioProperties;

        this.fileMap = Caffeine.newBuilder()
                .expireAfterAccess(FILE_CACHE_HOUR, TimeUnit.HOURS)
                .build();
    }

    @Scheduled(fixedDelay = 1000)
    public void listen() throws InterruptedException, MissingEventsException, IOException {
        EventBatch eventBatch = hdfsInputStream.take();

        for (Event event : eventBatch.getEvents()) {
            switch (event.getEventType()) {
                case CREATE: {
                    log.info("Create event {}", event);
                    Event.CreateEvent createEvent = (Event.CreateEvent) event;

                    if (createEvent.getPath().startsWith(hdfsProperties.getDirectory())
                            && createEvent.getiNodeType().equals(Event.CreateEvent.INodeType.FILE)) {
                        KafkaMessage kafkaMessage = KafkaMessage.builder()
                                .event(event.getEventType().name())
                                .id(UUID.randomUUID())
                                .source(createEvent.getPath())
                                .bucket(minioProperties.getBucket())
                                .timestamp(createEvent.getCtime())
                                .build();

                        processNewKafkaMessage(kafkaMessage);
                    }
                    break;
                }
                case UNLINK: {
                    Event.UnlinkEvent unlinkEvent = (Event.UnlinkEvent) event;
                    log.info("Unlink file {} ", unlinkEvent.getPath());

                    if (unlinkEvent.getPath().startsWith(hdfsProperties.getDirectory())) {
                        KafkaMessage kafkaMessage = KafkaMessage.builder()
                                .event(event.getEventType().name())
                                .id(UUID.randomUUID())
                                .bucket(minioProperties.getBucket())
                                .source(unlinkEvent.getPath())
                                .timestamp(unlinkEvent.getTimestamp())
                                .build();

                        kafkaService.send(kafkaMessage);
                    }
                    break;
                }
                case CLOSE: {
                    Event.CloseEvent closeEvent = (Event.CloseEvent) event;
                    log.info("Close event for file {} with size {}", closeEvent.getPath(), closeEvent.getFileSize());

                    Map<String, KafkaMessage> map = fileMap.asMap();

                    if (map.containsKey(closeEvent.getPath())) {
                        kafkaService.send(map.get(closeEvent.getPath()));
                        map.remove(closeEvent.getPath());
                    }
                    break;
                }
                default: {
                    log.info("Event type {}", event.getEventType().name());
                }
            }
        }
    }

    private void processNewKafkaMessage(KafkaMessage message) {
        Map<String, KafkaMessage> map = fileMap.asMap();

        if (map.containsKey(message.getSource())) {
            map.replace(message.getSource(), message);
        } else {
            fileMap.put(message.getSource(), message);
        }
    }
}
