package ru.itis.notifier.service;

import ru.itis.notifier.model.KafkaMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class KafkaService {

    @Value("${kafka.topic}")
    private String kafkaTopic;

    private final KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    public void send(KafkaMessage kafkaMessage) {
        log.info("Prepared to send :" + kafkaMessage);
        kafkaTemplate.send(kafkaTopic, kafkaMessage);
    }
}
