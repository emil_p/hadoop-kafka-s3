package ru.itis.notifier.configuration;

import java.io.IOException;

import ru.itis.notifier.properties.HdfsProperties;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hdfs.DFSClient;
import org.apache.hadoop.hdfs.DFSInotifyEventInputStream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Configuration
public class HdfsConfiguration {

    private final HdfsProperties hdfs;

    @Bean
    public org.apache.hadoop.conf.Configuration configuration() {
        System.setProperty("HADOOP_USER_NAME", hdfs.getUser());
        org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
        configuration.set("fs.defaultFS", hdfs.getUrl());
        return configuration;
    }

    @Bean
    public FileSystem fileSystem() throws IOException {
        return FileSystem.get(configuration());
    }

    @Bean
    public DFSClient hdfsClient() throws IOException {
        return new DFSClient(fileSystem().getUri(), configuration());
    }

    @Bean
    public DFSInotifyEventInputStream hdfsInputStream(DFSClient dfsClient) throws IOException {
        return dfsClient.getInotifyEventStream(0);
    }

}
