package ru.itis.notifier.model;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class KafkaMessage {

    private UUID id;

    private String event;

    private String source;

    private String bucket;

    private Long timestamp;
}
