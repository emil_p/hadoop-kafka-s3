# HDFS-S3 Replicator

### Modules:
* hdfs
    * Contains swagger (localhost:8081/swagger-ui/) with methods to save or delete files from HDFS
    * Upload files from volume to HDFS
* notifier
    * Catches all events with changes in HDFS
    * Produces Kafka messages to further upload files in S3 (Minio) if they`re fully downloaded
    * Produces Kafka messages to delete files from S3 (Minio)
* minio_pusher
    * Catches Kafka messages and downloads files from HDFS to S3 using temporary storage 
    * Automatically creates the bucket in S3 if it doesn`t exist

### Deploy

1. Check all volumes in application.yaml and docker-compose change them if needed
    * by default, create ./resources folder and add files to it
2. Create a folder for the hdfs container with files for further uploading to S3
3. Run maven clean package in the root module
4. Deploy all containers by docker-compose
    * create docker network cloud firstly

### Run
* Go to Swagger and create a POST request in the file controller. 
    * You have to use the path file from the hdfs volume folder (e.g., /resources/file1.txt)
* Wait until the file will be uploaded in the S3
    * Modules create logs to check the current state of the file uploading or deleting
    * To see logs go to the container console  
* Check the file size and the name in the S3