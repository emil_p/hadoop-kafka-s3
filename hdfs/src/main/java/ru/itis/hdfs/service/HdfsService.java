package ru.itis.hdfs.service;

import java.io.File;
import java.io.IOException;

import ru.itis.hdfs.properties.HdfsProperties;
import ru.itis.hdfs.model.FileDto;
import ru.itis.hdfs.model.FileForm;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class HdfsService {

    private final HdfsProperties hdfsProperties;

    private final Configuration configuration;

    public FileDto uploadFileToHdfs(FileForm form) {
        File file = new File(form.getSource());
        log.info("Start to upload file to hdfs {} with size {}", file.getName(), file.length());

        try (
                FileSystem fs = FileSystem.get(configuration)
        ) {
            fs.copyFromLocalFile(
                    new Path(file.getAbsolutePath()),
                    new Path(hdfsProperties.getDirectory().concat("/").concat(file.getName()))
            );
        } catch (IOException e) {
            log.error("Failed to upload file to hdfs, reason - {}", e.getMessage());
            throw new RuntimeException(e);
        }

        log.info("File {} is successfully uploaded to hdfs", file.getName());

        return FileDto.builder()
                .path(file.getPath())
                .size(file.length())
                .build();
    }

    public void deleteFileFromHdfs(FileForm form) {
        File file = new File(form.getSource());
        log.info("Start to delete file from hdfs {} with size {}", file.getPath(), file.length());

        try (
                FileSystem fs = FileSystem.get(configuration)
        ) {
            Path path = new Path(hdfsProperties.getDirectory().concat("/").concat(file.getName()));
            fs.delete(path, false);
        } catch (IOException e) {
            log.error("Failed to delete file from hdfs, reason - {}", e.getMessage());
            throw new RuntimeException(e);
        }

        log.info("File was deleted from hdfs {} with size {}", file.getPath(), file.length());
    }
}
