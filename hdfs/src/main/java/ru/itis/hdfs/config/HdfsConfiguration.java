package ru.itis.hdfs.config;

import ru.itis.hdfs.properties.HdfsProperties;

import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.hdfs.DistributedFileSystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Configuration
public class HdfsConfiguration {

    private final HdfsProperties hdfs;

    @Bean
    public org.apache.hadoop.conf.Configuration configuration() {
        System.setProperty("HADOOP_USER_NAME", hdfs.getUser());

        org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
        configuration.set("fs.defaultFS", hdfs.getUrl());
        configuration.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
        configuration.set("fs.file.impl", LocalFileSystem.class.getName());
        return configuration;
    }

}
