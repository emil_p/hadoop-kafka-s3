package ru.itis.hdfs.controller;

import ru.itis.hdfs.model.FileDto;
import ru.itis.hdfs.model.FileForm;
import ru.itis.hdfs.service.HdfsService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class FileController {

    private final HdfsService hdfsService;

    @PostMapping("/upload")
    public ResponseEntity<FileDto> uploadFileToHdfs(@RequestBody FileForm fileForm) {
        return ResponseEntity.ok(hdfsService.uploadFileToHdfs(fileForm));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Boolean> deleteFileFromHdfs(@RequestBody FileForm fileForm) {
        hdfsService.deleteFileFromHdfs(fileForm);
        return ResponseEntity.ok(Boolean.TRUE);
    }

}
